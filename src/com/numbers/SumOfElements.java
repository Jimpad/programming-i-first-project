package com.numbers;

public class SumOfElements {

    static int sum (int [] elements) {

        int sum = 0;

        for (int i = 0; i < elements.length; i++) sum += elements[i];

        return sum;
    }
}