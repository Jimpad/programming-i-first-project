package com.numbers;

public class BiggestNumber {

    public static int biggestNo (int [] elements) {

        int tmp = 0;

        for (int i = 0; i < elements.length; i++) {

            if (elements[i] > tmp) {

                tmp = elements[i];
            }
        }

        return tmp;
    }
}