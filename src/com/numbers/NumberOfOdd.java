package com.numbers;

public class NumberOfOdd {

    public static int getNumberOfOddNumbers (int [] elements) {

        int countOdd=0;

        for(int i=0; i < elements.length; i++) {

            if (elements[i] % 2 != 0) {

                countOdd++;
            }
        }

        return countOdd;
    }
}