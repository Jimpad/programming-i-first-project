// Goal is to find the biggest Palindrome that is not greater than the greatest element in the array

package com.numbers;

import java.util.Arrays;

public class BiggestPalindrome {

    public static boolean check_palindrome(int n) {
        int div = 1;

        for (int i = 0; i < n; i++) {

            while (n / div >= 10)
                div *= 10;

            while (n != 0) {
                int first = n / div;
                int last = n % 10;

// If first and last digits are not same then return false
                if (first != last)
                    return false;

// Removing the leading and trailing digits from the number
                n = (n % div) / 10;
// Reducing divisor by a factor of 2 as 2 digits are dropped
                div = div / 100;
            }
        }
        return true;
    }


    public static int largestPalindrome(int [] elements) {

        Arrays.sort(elements);

        int result = 0;

        int maxArray = 0;

        for (int element : elements) {

            if (element > maxArray) {

                maxArray = element;
            }
        }

        for (int i = 0; i < elements.length; i++) {

            for (int j = elements.length - 1; i >= 0; i--) {

                if (check_palindrome(elements[j])) {



                        result = elements[i];

                }
            }
        }
        return result;
    }

}