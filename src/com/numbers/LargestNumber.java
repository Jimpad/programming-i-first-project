package com.numbers;

public class LargestNumber {

    static int largestNumber(int [] elements) {

        int max = elements[0];

        for (int i = 0; i < elements.length; i++) {

            if (max > elements[i]) {

                max = max + elements[i];
            }
        }

        return max;
    }
}
