package com.numbers;

public class NumberOfPalindromeNumbers {

    static int getNumberOfPalindromeNumbers (int [] elements) {

        int count = 0;
        for (int i = 0; i < elements.length; i++) {

            int number = elements[i];
            int reversedNumber = 0;
            int temp = 0;

            while (number > 0) {

                temp = number % 10;
                number = number / 10;
                reversedNumber = reversedNumber * 10 + temp;
            }

            if (elements[i] == reversedNumber) {

                count++;
            }
        }

        return count;
    }
}
