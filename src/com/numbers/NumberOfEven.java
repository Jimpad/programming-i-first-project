package com.numbers;

public class NumberOfEven {

    public static int getNumberOfEvenNumbers (int [] elements) {

        int countEven=0;

        for(int i=0; i < elements.length; i++) {

            // 0 value for first element in array counts as even --> we have +1 even
            if (elements[i] % 2 == 0) {

                countEven++;
            }
        }

        return countEven;
    }
}