package com.numbers;

import java.util.Scanner;

public class NumberOfElements {

    public static int[] getElements() {

        Scanner sc = new Scanner (System.in);

        System.out.println("Declare array length: ");
        int elementsLength = sc.nextInt();

        int[] elements = new int[elementsLength];
        for(int i = 0; i < elements.length; i++) {

            System.out.println("Insert the next element: ");
            elements[i] = sc.nextInt();
        }

        sc.close();

        return elements;
    }
}