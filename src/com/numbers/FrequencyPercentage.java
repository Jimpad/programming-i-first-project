package com.numbers;

import java.util.Arrays;

public class FrequencyPercentage {

    static void getFrequencyPercentage (int [] elements) {

        int n = elements.length;
        boolean visited[] = new boolean[n];

        Arrays.fill(visited, false);

        // Traverse through array elements and
        // count individual frequencies
        for (int i = 0; i < n; i++) {

            // If already processed, skip.
            if (visited[i])
                continue;

            // Count the frequency (double) then divide it with length - n and multiply with 100 for percentage output
            double count = 1;

            int tmp = 0;
            int sum = 0;

            for (int j = i + 1; j < n; j++) {

                if (elements[i] == elements[j]) {

                    visited[j] = true;
                    count++;
                    tmp += count;
                }
            }

            // Cast to integer to avoid decimals when counting the number of times rather than percentages
            System.out.println("The element's: '" + elements[i] + "' occurrence is: " + ((count / n) * 100) + " %\nAnd its count is: " + (int) count + " times!\n");
        }
    }
}

