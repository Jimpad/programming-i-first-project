package com.numbers;

public class AverageOfAllNumber {

    static int average (int [] elements) {

        // Find sum of array element
        int sum = 0;

        for (int i = 0; i < elements.length; i++) sum += elements[i];

        return sum / elements.length;
    }
}