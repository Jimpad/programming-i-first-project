package com.numbers;

public class StandardDeviation {

    static double getStandardDeviation (int [] elements) {

        double sum = 0.0, standardDeviation = 0.0;
        int length = elements.length;

        for (double num : elements) {

            sum += num;
        }

        double mean = sum / length;

        for (double num : elements) {

            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation / length);
    }
}
