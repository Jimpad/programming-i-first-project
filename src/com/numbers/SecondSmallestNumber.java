package com.numbers;

public class SecondSmallestNumber {

    static int getSecondSmallest (int [] elements) {

        int temp;
        int total = elements.length;

        for (int i = 0; i < total; i++) {

            for (int j = i + 1; j < total; j++) {

                if (elements[i] > elements[j]) {

                    temp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = temp;
                }
            }
        }

        return elements[1];
    }
}