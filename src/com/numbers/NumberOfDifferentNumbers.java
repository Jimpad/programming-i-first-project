package com.numbers;

public class NumberOfDifferentNumbers {

    public static int differentNumbers (int [] elements){

        // We also observe 0 as different than other numbers, so we start with 1
        // to avoid future problems
        int values = 1;

        if(elements.length < 2) {

            //different of numbers
            return values;

        }else if( elements.length == 2) {

            if(elements[0] == elements[1]) {

                return values;
            }

            return 1;

        }else {

            for (int i = 1; i < elements.length; i++) {

                if (elements[i] == elements[i - 1]) {

                    continue;
                }else {

                    values++;
                }
            }

            return values;
        }
    }
}