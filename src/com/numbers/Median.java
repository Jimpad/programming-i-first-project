package com.numbers;

public class Median {

    static double getMedian (int [] elements) {

        double median;

        if (elements.length % 2 == 0) {

            median = ((double) elements [elements.length / 2] + (double) elements [elements.length / 2 - 1]) / 2;
        }
        else median = elements [elements.length / 2];

        return median;
    }
}
