package com.numbers;

import static com.numbers.AverageOfAllNumber.average;
import static com.numbers.FrequencyPercentage.getFrequencyPercentage;
import static com.numbers.Median.getMedian;
import static com.numbers.NumberOfDifferentNumbers.differentNumbers;
import static com.numbers.NumberOfEven.getNumberOfEvenNumbers;
import static com.numbers.NumberOfNumbers.getArray;
import static com.numbers.NumberOfOdd.getNumberOfOddNumbers;
import static com.numbers.NumberOfPalindromeNumbers.getNumberOfPalindromeNumbers;
import static com.numbers.StandardDeviation.getStandardDeviation;
import static com.numbers.SumOfElements.sum;

public class ReverseDataPrint {

    public static void printReverse (int[] elements) {

        System.out.print(getNumberOfPalindromeNumbers(elements));
        System.out.print(", ");
        System.out.print(sum(elements));
        System.out.print(", ");
        System.out.print(getMedian(elements));
        System.out.print(", ");
        System.out.print(getStandardDeviation(elements));
        System.out.print(", ");
        System.out.print(average(elements));

        // Breaking the line so it's easier to read. Method already breaks lines but is printed
        // in desired way
        System.out.print(", \n");
        getFrequencyPercentage(elements);
        System.out.print(", ");
        System.out.print(getNumberOfOddNumbers(elements));
        System.out.print(", ");
        System.out.print(getNumberOfEvenNumbers(elements));
        System.out.print(", ");
        System.out.print(differentNumbers(elements));
        System.out.print(", [");
        getArray(elements);
        System.out.print("]");

    }
}
