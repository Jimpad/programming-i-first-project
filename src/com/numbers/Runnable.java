// Author: Dino Hadžić - 89181114

// Here we can test out everything to make sure it works

package com.numbers;

import static com.numbers.NumberOfElements.getElements;
import static com.numbers.NumberOfNumbers.getArray;
import static com.numbers.NumberOfDifferentNumbers.differentNumbers;
import static com.numbers.NumberOfEven.getNumberOfEvenNumbers;
import static com.numbers.NumberOfOdd.getNumberOfOddNumbers;
import static com.numbers.FrequencyPercentage.getFrequencyPercentage;
import static com.numbers.MostFrequentElement.mostFrequent;
import static com.numbers.BiggestNumber.biggestNo;
import static com.numbers.SecondSmallestNumber.getSecondSmallest;
import static com.numbers.AverageOfAllNumber.average;
import static com.numbers.StandardDeviation.getStandardDeviation;
import static com.numbers.Median.getMedian;
import static com.numbers.SumOfElements.sum;
import static com.numbers.NumberOfPalindromeNumbers.getNumberOfPalindromeNumbers;
import static com.numbers.BiggestPalindrome.largestPalindrome;
import static com.numbers.ReverseDataPrint.*;

public class Runnable {

    public static void main (String [] args) {

        // Initialize the array
        int[] arr = getElements();

        // 1
        System.out.println("Number of elements (numbers): \n");
        getArray(arr);

        // 2
        System.out.println("\nNumber of different numbers: ");
        System.out.println(differentNumbers(arr));

        // 3
        System.out.println("Number of even numbers: ");
        System.out.println(getNumberOfEvenNumbers(arr));

        // 4
        System.out.println("Number of odd numbers: ");
        System.out.println(getNumberOfOddNumbers(arr));

        // 5
        System.out.println("Frequency in percentage for each number: \n");
        getFrequencyPercentage(arr);

        // 6
        System.out.println("\nMost frequent element: ");
        System.out.println(mostFrequent(arr));

        // 7
        System.out.println("Biggest number: ");
        System.out.println(biggestNo(arr));

        // 8
        System.out.println("Second smallest number: ");
        System.out.println(getSecondSmallest(arr));

        // 9
        System.out.println("Average of all numbers: ");
        System.out.println(average(arr));

        // 10
        System.out.println("Standard deviation: ");
        System.out.println(getStandardDeviation(arr));

        // 11
        System.out.println("Median: ");
        System.out.println(getMedian(arr));

        // 12
        System.out.println("Sum of all numbers: ");
        System.out.println(sum(arr));

        // 13
        System.out.println("Number of palindromic elements: ");
        System.out.println(getNumberOfPalindromeNumbers(arr));;

        // 13 Largest palindromic number, that is smaller than the greatest num in array
            // System.out.println();
            // System.out.println(largestPalindrome(arr));

        // 14
        System.out.println("\nReverse data print, delimited by comma: \n");
        printReverse(arr);
    }
}
