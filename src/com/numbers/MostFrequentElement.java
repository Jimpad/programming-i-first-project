package com.numbers;

import java.util.Arrays;

public class MostFrequentElement {

    static int mostFrequent (int[] elements) {

        Arrays.sort(elements);

        // Find the most frequent number using linear traversal
        int maxCount = 1;
        int currentCount = 1;
        int res = elements[0];
        int n = elements.length;

        for(int i = 1; i < n; i++) {

            if(elements[i] == elements[i - 1]) {

                currentCount++;
            }else{

                if (currentCount > maxCount) {

                    maxCount = currentCount;
                    res = elements[i - 1];
                }

                currentCount = 1;
            }
        }

        if(currentCount > maxCount) {

            maxCount = currentCount;
            res = elements[n - 1];
        }

        return res;
    }
}